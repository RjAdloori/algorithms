'use strict';

class List {

  constructor() {
    this.memory = [];
    this.length = 0;
  }

  get(address) {
    if(address < 0 || address > this.length-1) return null;
    return this.memory[address]
  }

  push(value) {
    this.memory[this.length] = value;
    this.length++;
  }

  pop() {
    if(this.length === 0) return;

    let value = this.memory[this.length-1];
    delete this.memory[this.length-1];
    this.length--;
    return value;
  }

  unshift(value) {
    let previous = value;

    for(let i=0; i<this.length; i++) {
      current = this.memory[i];
      this.memory[i] = previous;
      previous = current;
    }

    this.memory[this.length] = previous;
    this.length++;
  }

  shift() {
    if(this.length === 0) return;

    let value = this.memory[0];
    
    for(int i=0; i<this.length-1; i++) {
      this.memory[i] = this.memery[i+1];
    }

    delete this.memory[this.length-1];
    this.length--;

    return value;
  }
}

class HashTable {
  constructor {
    this.memory = [];
  }

  hashKey(key) {
    let hash = 0;
    for (let index = 0; index < key.length; index++) {
      let code = key.charCodeAt(index);
      hash = ((hash << 5) - hash) + code | 0;
    }
    return hash;
  }

  get(key) {
    let address = hashKey(key);
    return this.memery[address];
  }

  set(key, value) {
    let address = hashKey(key);
    this.memory[address] = value;
  }

  remove(key) {
    let address = hashKey(key);
    if(this.memory[address]) {
      delete this.memory[address];
    }
  }
}

class Stack {
  constructor() {
    this.memory = [];
    this.length = 0;
  }

  push(value) {
    this.memory[this.length] = value;
    this.length++;
  }

  pop() {
    if(this.length === 0) return null;

    let value = this.memory[this.length-1];
    delete this.memory[this.length-1];
    this.length--;

    return value;
  }

  peek() {
    return this.memory[this.length-1];
  }
}

class Queue {
  constructor() {
    this.memory = [];
  }

  enqueue(value) {
    this.memory.push(value);
  }

  dequeue() {
    if(this.memory.length === 0) return;
    return this.memory.shift(); 
  }

  peek() {
    return this.memory[0];
  }
}

