/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 * 
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 * Explanation: 342 + 465 = 807.
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
  let borrow = 0;
  let result = new ListNode(0);
  let current = result;
  
  while(l1 !== null || l2 !== null) {
      let sum = 0;
      
      if(l1) {
          sum += l1.val;
          l1 = l1.next;
      }
      if(l2) {
          sum += l2.val;
          l2 = l2.next;
      }
      
      sum += borrow;
      borrow = sum > 9? 1 : 0;
      sum = sum%10;
      
      current.next = new ListNode(sum);
      current = current.next;
  }
  
  if(borrow) {
      current.next = new ListNode(1);
  }
  
  return result.next;
  
};